#include <iostream>
#include <vector>
#include <random>

using namespace std;

int main() {

	vector<char> a = { '1','2','1','0' };
	vector<char> b = { 'C','O','M','P' };

	random_device rd;
	mt19937 eng(rd());
	mt19937 eng1(rd());
	uniform_int_distribution <int> norm(1, 4);


	while (norm(eng) == 1) {

		a.pop_back();
		a.push_back('P');

		cout << a[0] << a[1] << a[2] << a[3] << " ERROR" << endl;
		norm(eng);
	}
	while (norm(eng) == 2) {
		a.pop_back();
		a.pop_back();
		a.push_back('M');
		a.push_back('P');

		cout << a[0] << a[1] << a[2] << a[3] << " ERROR" << endl;
		norm(eng);
	}
	while (norm(eng) == 3) {
		a.pop_back();
		a.pop_back();
		a.pop_back();
		a.push_back('O');
		a.push_back('M');
		a.push_back('P');

		cout << a[0] << a[1] << a[2] << a[3] << " ERROR" << endl;
		norm(eng);
	}
	while (norm(eng) != 4) {

		norm(eng);

		if (norm(eng) == 4) {

			a.pop_back();
			a.pop_back();
			a.pop_back();
			a.pop_back();
			a.push_back('C');
			a.push_back('O');
			a.push_back('M');
			a.push_back('P');

			cout << a[0] << a[1] << a[2] << a[3] << endl;
		}

	}

	while (norm(eng1) == 1) {

		b.pop_back();
		b.push_back('1');
		cout << b[0] << b[1] << b[2] << b[3] << " ERROR" << endl;
		norm(eng1);
	}
	if (norm(eng1) == 2) {
		b.pop_back();
		b.pop_back();
		b.push_back('0');
		b.push_back('1');
		cout << b[0] << b[1] << b[2] << b[3] << " ERROR" << endl;
		norm(eng1);
	}
	if (norm(eng1) == 3) {
		b.pop_back();
		b.pop_back();
		b.pop_back();
		b.push_back('2');
		b.push_back('0');
		b.push_back('1');
		cout << b[0] << b[1] << b[2] << b[3] << " ERROR" << endl;
		norm(eng1);
	}

	while (norm(eng1) != 4) {

		norm(eng1);

		if (norm(eng1) == 4) {
			b.pop_back();
			b.pop_back();
			b.pop_back();
			b.pop_back();
			b.push_back('1');
			b.push_back('2');
			b.push_back('1');
			b.push_back('0');

			cout << b[0] << b[1] << b[2] << b[3] << endl;
		}
	}
	if (((a[0] != 'C') && (a[1] != 'O') && (a[2] != 'M') && (a[3] != 'P')) && ((b[0] != '1') && (b[1] != '2') && (b[2] != '0') && (b[3] != '1'))){
		
		cout << " ERROR Please re-run the simulation program..." << endl;
		cout << a[0] << a[1] << a[2] << a[3] << b[0] << b[1] << b[2] << b[3] << endl;
	} else {

		cout << a[0] << a[1] << a[2] << a[3] << b[0] << b[1] << b[2] << b[3] << endl;
	}
}
